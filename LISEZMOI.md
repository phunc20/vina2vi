1. change version in pyproject.toml
2. python -m build
3. rm old versions in dist/
4. twine upload -r testpypi dist/*
5. twine upload dist/*

https://setuptools.pypa.io/en/latest/userguide/quickstart.html
https://martin-thoma.com/pyproject-toml/
https://packaging.python.org/en/latest/guides/using-testpypi/
https://pypi.org/project/twine/
https://twine.readthedocs.io/en/stable/
https://test.pypi.org/project/vina2vi/
https://pypi.org/project/vina2vi/

- dependencies
    - <https://setuptools.pypa.io/en/latest/userguide/dependency_management.html>
