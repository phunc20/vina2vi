- [ ] `Vietnamese` class attributes could be modified by users. No good?  
      Let's see how other packages' behave:
      ```python
      In [1]: import numpy as np
      
      In [2]: np.pi
      Out[2]: 3.141592653589793
      
      In [3]: np.pi = 2.718281828459045
      
      In [4]: np.pi
      Out[4]: 2.718281828459045
      ```
- [x] Problem with package, module, etc.
      ```python
      In [2]: dir(vina2vi)
      Out[2]:
      ['__builtins__',
       '__cached__',
       '__doc__',
       '__file__',
       '__loader__',
       '__name__',
       '__package__',
       '__path__',
       '__spec__',
       'metrics',
       'models',
       'util',
       'vina2vi']
      
      In [7]: vina2vi.models is vina2vi.vina2vi.models
      Out[7]: True
      
      In [8]: vina2vi is vina2vi.vina2vi
      Out[8]: True
      ```
    - It seems that we should not have put the following in `vina2vi/__init__.py`
      ```python
      import vina2vi.util
      import vina2vi.metrics
      import vina2vi.models
      ```
- [x] Make sure packages are installed via `pyproject.toml` (and `requirements{,_dev}.txt`), e.g.
      lacking `pandas`
- [ ] Add GitLab CICD for testing `vina2vi` running ok on all sorts of platforms
- [ ] Add `vina2vi.__version__`, `vina2vi.__git_version__`, etc. (from `__init__.;py` perhaps) (Cf. NumPy, TensorFlow, etc.)
