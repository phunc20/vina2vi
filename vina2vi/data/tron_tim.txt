Có một cái cây trong một cái vườn.
Trên những tán cây nở rộ những đoá hoa.
Có hai đứa nhóc đang chơi trốn tìm.
Tìm hoài tìm mãi nên quên lối về.
Anh đi tìm thì em trốn anh đi trốn em không tìm.
Lòng em không gợn sóng cuối cùng anh mất công chìm.
Nếu mà có cây búa anh sẽ nện cho bõ công.
Vì nhớ nhung đặc quánh giờ nó khô thành bê tông.
Chúng ta rồi sẽ có có những chuyến đi dài.
Ta tự học lấy mọi thứ vì trong tình trường làm biếng ghi bài.
Câu chuyện của chúng ta bỗng có thêm một miếng bi hài.
Vì cách mà em gọi anh không có u và thiếu y dài.
Phía xa thành phố người ta đã bật đèn đường.
Hương quen xưa làm anh thấy thật thèm thuồng.
Nỗi buồn vàng rực cứ như là chứa đồng thau.
Hệt như là Beckham vậy em chỉ giỏi cứa lòng nhau.
Hé cửa sổ ra mà xem:
Có một chàng thi sĩ đứng ở ngay nhà em.
Viết nhạc tình mát ngọt tựa như cây cà rem.
Anh ta sẽ đứng ở nơi đây cả đêm.
Có hai cái cây trong một cái vườn (có hai cái cây trong vườn).
Trên những tán cây nở rộ những đoá hoa (có những đoá hoa trên cành).
Có hai đứa nhóc đang chơi trốn tìm (có hai đứa nhóc đang chơi).
Tìm hoài tìm mãi nên quên lối về.
Hồi đó anh rụt rè như đám cây mắc cỡ.
Gần em làm anh hồi hộp tới mức gây tắc thở.
Ta đều không biết có điều gì sau đám mây sắp nở.
Trò chơi trốn tìm ngày đó sau này đầy trắc trở.
Ta săn bắn những khát vọng và hái lượm những giấc mơ.
Ta gieo trồng cây ước mộng thứ mà lấy đi nhiều thì giờ.
Ta đào những cái hố mà không biết có ngày bị lọt.
Để rất lâu sau này chúng ta cau mày nhận ra không phải tất cả bông hoa thì đều sẽ có những nhụy ngọt.
Gọi tên em làm anh mất giọng hoài.
Hệt như là một giấc mộng dài.
Ta đi tìm cả thế giới nhưng mà lại trốn nhau.
Biết vậy thà nghỉ chơi từ lúc mới chớm đau (từ lúc mới chớm đau).
Người đi tìm cái người đi trốn.
Người đi trốn cái người đi tìm.
Tình yêu từ những ngày xưa đã
ngủ quên dưới tán cây im lìm.
Có ba cái cây trong một cái cái vườn.
Trên những tán cây nở rộ những đoá hoa.
Có hai đứa nhóc đang chơi trốn tìm.
Tìm hoài tìm mãi nên quên lối về.
Ai đó đã chặt hết cây và cũng chẳng thấy vườn (chẳng thấy cây trong vườn).
Ai đó đã xây một ngôi nhà rất to (chặt hết cây đi rồi).
Chắc em hạnh phúc yên vui với người (chắc em hạnh phúc yên vui).
Nhà em có chó anh không dám vào.
Niềm cô đơn của những người trưởng thành:
Là khi muốn trốn nhưng không ai tìm.
Nhiều khi ta muốn ta được bé lại
để khi đi trốn có người đi tìm.
Nhiều khi ta muốn ta được bé lại
để khi đi trốn có người đi tìm.
