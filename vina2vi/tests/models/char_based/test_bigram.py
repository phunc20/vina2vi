import pytest

from vina2vi.models.char_based.bigram import Bigram


@pytest.mark.parametrize("s", [
    "Lãnh đạo Chính phủ mặc niệm nạn nhân tử vong vụ cháy chung cư mini",
    "Hai ngân hàng có vốn nhà nước là Vietcombank và Agribank vừa hạ lãi suất tiết kiệm về 5,5% một năm",
    "Thao duoc ho tro giup an than, giam cang thang than kinh lay lai giac ngu ngon moi toi.",
])
@pytest.mark.parametrize("stochastic", [True, False])
def test_translation_length(s, stochastic):
    bigram = Bigram()
    bigram.fit([])
    translation = bigram.translate(s, stochastic=stochastic)
    assert len(translation) == len(s)



